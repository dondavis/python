## Functions

### **Introduction:**
This lesson takes a look into the creation and use of Python functions.

### **Topics Covered:**

* [**Scope**](https://90cos.gitlab.io/public/training/lessons/python/functions/scope.html#scope)
* [**User Functions**](https://90cos.gitlab.io/public/training/lessons/python/functions/user_functions.html#user-functions)
* [**Parameters and Arguments**](https://90cos.gitlab.io/public/training/lessons/python/functions/user_functions.html#parameters-and-arguments)
* [**Commandline Arguments**](https://90cos.gitlab.io/public/training/lessons/python/functions/user_functions.html#cmdline-arguments)
* [**Returning**](https://90cos.gitlab.io/public/training/lessons/python/functions/user_functions.html#returning)
* [**Pass By Reference**](https://90cos.gitlab.io/public/training/lessons/python/functions/user_functions.html#pass-by-reference)
* [**Lambda Functions**](https://90cos.gitlab.io/public/training/lessons/python/functions/lambda_functions.html#lambda-functions)
* [**List Comprehension**](https://90cos.gitlab.io/public/training/lessons/python/functions/list_comprehension.html#list-comprehension)
* [**Closures**](https://90cos.gitlab.io/public/training/lessons/python/functions/closures_iterators_generators.html#closures-iterators--generators)
* [**Iterators**](https://90cos.gitlab.io/public/training/lessons/python/functions/closures_iterators_generators.html#iterators)
* [**Generators**](https://90cos.gitlab.io/public/training/lessons/python/functions/closures_iterators_generators.html#generators)

#### To access the Functions slides please click [here](https://90cos.gitlab.io/public/training/lessons/python/functions/slides)

