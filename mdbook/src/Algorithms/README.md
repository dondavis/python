## Intro To Algorithms

### **Introduction:**
In this section, the Student will learn to implement complex data structures and algorithms using Python

Alogrithm and Data structures allow you to store and organize data efficiently. 
They are critical to any problem, provide a complete solution, and act like reusable code. 
Overall, this Introduction to Algorithms with Python will teach you the essential Python data structures and the most common
algorithms for building easy and maintainable applications.

### **Topics Covered:**

* [**Algorithms Analysis**](https://90cos.gitlab.io/public/training/lessons/python/Algorithms/Algorithm_Analysis.html)
* [**Algorithms Design**](https://90cos.gitlab.io/public/training/lessons/python/Algorithms/Algorithm_Design.html)
* [**Big O Notation**](https://90cos.gitlab.io/public/training/lessons/python/Algorithms/Big_O_notation.html)
* [**Searching Algorithms**](https://90cos.gitlab.io/public/training/lessons/python/Algorithms/Searching_Algorithms.html)
* [**Sorting Algorthms**](https://90cos.gitlab.io/public/training/lessons/python/Algorithms/Sort.html)
* [**List Pointer Structures**](https://90cos.gitlab.io/public/training/lessons/python/Algorithms/Lists_Pointer_Structures.html)
* [**Linked Lists**](https://90cos.gitlab.io/public/training/lessons/python/Algorithms/Singly_Linked_List.html)
* [**Stacks**](https://90cos.gitlab.io/public/training/lessons/python/Algorithms/Stacks_Lesson.html)
* [**Queue**](https://90cos.gitlab.io/public/training/lessons/python/Algorithms/Queue_Lesson.html)
* [**Trees**](https://90cos.gitlab.io/public/training/lessons/python/Algorithms/Trees_Lesson.html)

#### To access the Algorithms slides please click [here](https://90cos.gitlab.io/public/training/lessons/python/Algorithms/slides/)
