'''
Complete the function to meet the following requirements
* This will be accomplished three times using three different packages (os, glob and subprocess)
* The filename starts with "file" and the has a "txt" extension
- recursively search the additional_modules_dir directory
- Read the contents of each file
- Return the relative path and filename (with extension) of the file that contains the username "jenny" (no quotes)
- Example return: "additional_modules_dir\deep\deeper\file4839503.txt"
'''

import os
import glob
import subprocess


def find_file_with_os():
    pass


def find_file_with_glob():
    pass


def find_file_with_subprocess():
    pass
