## Advanced

### **Introduction:**
This lesson you will quickly dive into more advanced uses and features of Python.

### **Topics Covered:**

  * [**CTypes and Structures**](https://90cos.gitlab.io/public/training/lessons/python/advanced/ctypes.html#ctypes-and-structures-1)
  * [**Loading Libraries**](https://90cos.gitlab.io/public/training/lessons/python/advanced/ctypes.html#loading-libraries)
  * [**Structures**](https://90cos.gitlab.io/public/training/lessons/python/advanced/ctypes.html#structures)
  * [**Regular Expressions**](https://90cos.gitlab.io/public/training/lessons/python/advanced/regular_expressions.html#regular-expressions)
  * [**Additional Libraries and Modules**](https://90cos.gitlab.io/public/training/lessons/python/advanced/additional_libaries_modules.html)
  * [**Multithreading**](https://90cos.gitlab.io/public/training/lessons/python/advanced/multithreading.html#multithreading)
  * [**Unit Testing**](https://90cos.gitlab.io/public/training/lessons/python/advanced/unit_testing.html#unittesting)
  * [**Metaclasses**](https://90cos.gitlab.io/public/training/lessons/python/advanced/metaclasses.html#metaclasses)

#### To access the Advanced Python slides please click [here](https://90cos.gitlab.io/public/training/lessons/python/advanced/slides)
